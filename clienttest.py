import os
import requests

json={"name":"my new project", "visibility":"internal"}
requests.post(os.getenv("gitlab_url")+"/projects", auth=(os.getenv("gitlab:user"), os.getenv("gitlab_password")), data=json)